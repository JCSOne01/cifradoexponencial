/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cifradoexponencial;

/**
 *
 * @author Juan Carlos Sierra Rincón
 */
public class CifradoExponencial
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        System.out.println("========== Encriptar ==========");
        int[] perro = new int[]
        {
            1604, 1818, 1524
        };

        encriptar(perro, 17, 2707);

        System.out.println("========= Desencriptar ========");
        int[] perroEncriptado = new int[]
        {
            1499, 1079, 559
        };

        desencriptar(perroEncriptado, 17, 2707);

        System.out.println("========= Desencriptar ========");
        int[] tarea = new int[]
        {
            2088, 0345, 1868, 1808, 1868 , 864, 0145
        };

        desencriptar(tarea, 571, 3319);
    }

    public static void encriptar(int[] bases, int llave, int modulo)
    {
        for (int i = 0; i < bases.length; i++)
        {
            System.out.println(exponenteModular(bases[i], llave, modulo));
        }
    }

    public static void desencriptar(int[] bases, int llave, int modulo)
    {
        int potencia = congruenciaLineal(llave, 1, phi(modulo));
        for (int i = 0; i < bases.length; i++)
        {
            System.out.println(exponenteModular(bases[i], potencia, modulo));
        }
    }

    public static int exponenteModular(int b, int p, int m)
    {
        int x = 1;
        int pot = modulo(b, m);

        String potenciaBinariaInvertida = InvertirString.invertirString(
                Integer.toBinaryString(p));

        for (int i = 0; i < potenciaBinariaInvertida.length(); i++)
        {
            if (potenciaBinariaInvertida.charAt(i) == '1')
            {
                x = modulo(x * pot, m);
            }
            pot = modulo(pot * pot, m);
        }
        return x;
    }

    public static int modulo(int a, int m)
    {
        int modulo = a % m;
        if (modulo < 0)
        {
            return m + modulo;
        }
        else
        {
            return modulo;
        }
    }

    public static int congruenciaLineal(int a, int b, int m)
    {
        int respuesta = 0;

        if (b == 0)
        {
            return 0;
        }

        if (m < a)
        {
            a = modulo(a, m);
            b = modulo(b, m);
        }

        if (a == 1)
        {
            return b;
        }

        respuesta = congruenciaLineal(m, -b, a);

        return ((respuesta * m) + b) / a;
    }

    private static int phi(int n)
    {
        int x = 0;
        for (int i = 1; i <= n; i++)
        {
            if (MaxComDiv.maxComDiv(n, i) == 1)
            {
                x++;
            }
        }
        return x;
    }
}
