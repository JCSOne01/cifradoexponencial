/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cifradoexponencial;

/**
 *
 * @author Andres
 * http://www.eljavatar.com/2014/05/Como-Hallar-el-Maximo-Comun-Divisor-en-Java.html
 */
public class MaxComDiv
{

    public static int maxComDiv(int num1, int num2)
    {
        int a = Math.max(num1, num2);
        int b = Math.min(num1, num2);

        int res;

        // Creamos el ciclo que realizará las iteraciones
        do
        {
            // Le asignamos al resultado el valor de b.
            // Si la primera división da exacta, el mcd será
            // el menor de los dos, es decir "b". Por ejemplo
            // si los números son 6 y 3, "b" será igual a 3 y
            // será por consiguiente el mcd al ser 6%3=0.
            // De no ser así, "res" irá guardando el valor del
            // resto entre "a" y "b" de la anterior iteración
            res = b;
            // Le asignamos a la variable "b" el valor del resto
            // de la división entre "a" y "b", de tal forma que
            // "b" siempre se convertirá en el divisor de la
            // proxima iteración
            b = a % b;
            // El dividendo de la proxima iteración, es decir "a"
            // será entonces el resto de la anterior iteración
            a = res;
        }
        while (b != 0);

        return res;
    }
}
