/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cifradoexponencial;

/**
 *
 * @author Víctor Cuervo
 * http://lineadecodigo.com/java/invertir-una-cadena-con-reverse-en-java/
 */
public class InvertirString
{

    public static String invertirString(String cadena)
    {
        StringBuilder builder = new StringBuilder(cadena);
        return builder.reverse().toString();
    }
}
